//
//  FDWebService.m
//  maruko
//
//  Created by 王澍宇 on 16/2/22.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import "FDWebService.h"

@implementation FDWebService

+ (void)requestWithAPI:(NSString *)api Method:(NSString *)method Parms:(NSDictionary *)parms HUD:(BOOL)hud Block:(void (^)(BOOL, NSDictionary *))block {
    
    if (hud) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    }
    
    [[FDNetworkEngine sharedEngine] addSessionTaskWithAPI:api Method:method Parms:[parms copy] Callback:^(NSURLSessionDataTask *task, NSDictionary *reseponseDic, NSError *error) {
        
        [FDWebService actionWithReseponseDic:reseponseDic Error:error HUD:hud Block:block];
        
    }];
}

+ (void)actionWithReseponseDic:(NSDictionary *)reseponseDic Error:(NSError *)error HUD:(BOOL)hud Block:(void (^)(BOOL, NSDictionary *))block {
    
    BOOL isSuccess = error ? NO : YES;
    
    NSString *message = error ? error.domain : reseponseDic[@"msg"];
    
    if (hud) {
        if (isSuccess) {
            [SVProgressHUD showSuccessWithStatus:message];
        } else {
            [SVProgressHUD showErrorWithStatus:message];
        }
    }
    
    NSDictionary *resultDic = reseponseDic[kResultsKey];
    
    if ([resultDic isKindOfClass:[NSNull class]]) {
        resultDic = [NSDictionary new];
    }
    
    if (block) {
        block(isSuccess, isSuccess ? resultDic : nil);
    }
}

+ (void)requestWithAPI:(NSString *)api
                 Parms:(NSDictionary *)parms
                 Files:(NSArray<FDFile *> *)files
                   HUD:(BOOL)hud
                 Block:(void (^)(BOOL, NSDictionary *))block
      ProgressCallback:(FDProgressCallback)progressCallback {
    
    [[FDNetworkEngine sharedEngine] addSessionDataTaskWithAPI:api Parms:parms Files:files Callback:^(NSURLSessionDataTask *task, NSDictionary *responseDic, NSError *error) {
        
        [FDWebService actionWithReseponseDic:responseDic Error:error HUD:hud Block:block];
        
    } ProgressAction:^(float progress) {
        
        if (hud) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showProgress:progress status:@"正在上传" maskType:SVProgressHUDMaskTypeClear];
            });
        }
        
        if (progressCallback) {
            progressCallback(progress);
        }
    }];
}

@end
