//
//  FDProject.h
//  actor
//
//  Created by 王澍宇 on 16/3/22.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"
#import "FDPackage.h"
#import "FDDepartureTime.h"

@interface FDProject : FDBaseModel

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *intro;

@property (nonatomic, strong) NSString *applyPlace;

@property (nonatomic, assign) float applyFee;

@property (nonatomic, strong) NSArray<NSString *> *tags;

@property (nonatomic, strong) NSArray<FDPackage *> *packages;

@property (nonatomic, strong) NSArray<NSString *> *imageURLs;

@property (nonatomic, strong) NSArray<FDDepartureTime *> *departureTimes;

@property (nonatomic, strong) NSString *routeIntro;

@property (nonatomic, strong) NSString *feeIntro;

@property (nonatomic ,strong) NSString *bookIntro;

@property (nonatomic, strong) NSString *cancelIntro;

@property (nonatomic, strong) NSString *qaIntro;

@property (nonatomic, assign) BOOL hasFavoed;

@end
