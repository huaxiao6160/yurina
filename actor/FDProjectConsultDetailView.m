//
//  FDConsultDetailView.m
//  actor
//
//  Created by 王澍宇 on 16/4/3.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProjectConsultDetailView.h"
#import "FDAccountService.h"
#import "FDProjectService.h"

@implementation FDProjectConsultDetailView {
    
    UILabel *_nameLabel;
    UILabel *_telLabel;
    UILabel *_mailLabel;
    UILabel *_problemLabel;
    
    UITextField *_nameField;
    UITextField *_telField;
    UITextField *_mailField;
    UITextField *_problemField;
    
    UILabel *_tipLabel;
    
    FDButton *_confirmButton;
}

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info {
    if (self = [super initWithShadowColor:color Info:info]) {
        
        self.titleLabel.text = @"咨询客服";
        
        _nameLabel = [UILabel labelWithText:@"姓名 *" Color:ColorTripTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _telLabel = [UILabel labelWithText:@"电话 *" Color:ColorTripTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _mailLabel = [UILabel labelWithText:@"邮箱 *" Color:ColorTripTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        
        _problemLabel = [UILabel labelWithText:@"备注 *" Color:ColorTripTipText FontSize:10 Alignment:NSTextAlignmentLeft Light:YES];
        _problemLabel.hidden = YES;
        
        WeakSelf;
        
        _nameField = [UITextField new];
        _nameField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _nameField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"请输入姓名" Color:ColorTripTipText FontSize:16];
        _nameField.beginAction = ^(NSString *text, UITextField *field) {
            
            StrongSelf;
            
            [s_self moveHeigher];
        };
        
        _telField = [UITextField new];
        _telField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _telField.keyboardType = UIKeyboardTypeNamePhonePad;
        _telField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"请输入手机号码" Color:ColorTripTipText FontSize:16];
        
        _mailField = [UITextField new];
        _mailField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _mailField.keyboardType = UIKeyboardTypeEmailAddress;
        _mailField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"请输入常用邮箱" Color:ColorTripTipText FontSize:16];
        
        _problemField = [UITextField new];
        _problemField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _problemField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"请简要说明遇到的问题" Color:ColorTripTipText FontSize:16];
        _problemField.doneAction = ^(NSString *text, UITextField *field) {
            
            StrongSelf;
            
            [s_self moveLower];
        };
        
        _problemField.hidden = YES;
        
        ContentViewAddSubView(_nameLabel);
        ContentViewAddSubView(_telLabel);
        ContentViewAddSubView(_mailLabel);
        ContentViewAddSubView(_problemLabel);
        
        ContentViewAddSubView(_nameField);
        ContentViewAddSubView(_telField);
        ContentViewAddSubView(_mailField);
        ContentViewAddSubView(_problemField);
        
        UIView *firstline = [UIView new];
        firstline.backgroundColor = ColorTripTipText;
        
        UIView *secondLine = [UIView new];
        secondLine.backgroundColor = ColorTripTipText;
        
        UIView *thirdLine = [UIView new];
        thirdLine.backgroundColor = ColorTripTipText;
        
        UIView *fourthLine = [UIView new];
        fourthLine.backgroundColor = ColorTripTipText;
        fourthLine.hidden = YES;
        
        ContentViewAddSubView(firstline);
        ContentViewAddSubView(secondLine);
        ContentViewAddSubView(thirdLine);
        ContentViewAddSubView(fourthLine);
        
        _tipLabel = [UILabel labelWithText:@"提交信息后我们将尽快与您联系，请保持通信畅通"
                                     Color:ColorProjectlTipText
                                  FontSize:10
                                 Alignment:NSTextAlignmentCenter
                                     Light:NO];
        
        _confirmButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"提交联系信息" FontSize:14 ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            if ([FDAccountService checkIfNeedLogin]) {
                return [FDAccountService promptLogin];
            }
            
            [FDProjectService consultWithName:s_self->_nameField.text
                                          tel:s_self->_telField.text
                                         mail:s_self->_mailField.text
                                      problem:s_self->_problemField.text
                                     Callback:^(BOOL success) {
                                         if (success) {
                                             [s_self dismiss];
                                         }
                                     }];
            
        }];
        
        [_confirmButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        [_confirmButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:NO AutoSelected:NO];
        
        ContentViewAddSubView(_tipLabel);
        ContentViewAddSubView(_confirmButton);
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(self.firstLine.mas_bottom).offset(30);
        }];
        
        [_nameField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_nameLabel.mas_bottom).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [firstline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameField);
            make.top.equalTo(_nameField.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
        }];
        
        [_telLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(firstline.mas_bottom).offset(30);
        }];
        
        [_telField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_telLabel.mas_bottom).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [secondLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameField);
            make.top.equalTo(_telField.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
        }];
        
        [_mailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(secondLine.mas_bottom).offset(30);
        }];
        
        [_mailField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_mailLabel.mas_bottom).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [thirdLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameField);
            make.top.equalTo(_mailField.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
        }];
        
        [_problemLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(thirdLine.mas_bottom).offset(30);
        }];
        
        [_problemField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_nameLabel);
            make.top.equalTo(_problemLabel.mas_bottom).offset(10);
            make.right.equalTo(@(-20));
        }];
        
        [fourthLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(_nameField);
            make.top.equalTo(thirdLine.mas_bottom).offset(5);
            make.height.equalTo(@0.5);
        }];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(fourthLine.mas_bottom).offset(20);
            make.centerX.equalTo(@0);
        }];
        
        [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
            make.top.equalTo(_tipLabel.mas_bottom).offset(20);
            make.height.equalTo(@45);
            make.bottom.equalTo(@(-20));
        }];
        
    }
    return self;
}

@end
