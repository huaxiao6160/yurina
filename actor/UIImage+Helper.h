//
//  UIImageView+Helper.h
//  actor
//
//  Created by 王澍宇 on 16/3/18.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helper)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
