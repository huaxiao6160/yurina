//
//  FDLocation.h
//  actor
//
//  Created by 王澍宇 on 16/4/1.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"

@interface FDLocation : FDBaseModel

@property (nonatomic, strong) NSString *name;

@end
