//
//  FDTopic.h
//  actor
//
//  Created by 王澍宇 on 16/3/19.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"
#import "FDProject.h"
#import "FDTag.h"

@interface FDTopic : FDBaseModel

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *intro;

@property (nonatomic, strong) NSString *imageURL;

@property (nonatomic, strong) FDTag *tag;

@property (nonatomic, strong) NSArray<FDProject *> *projects;

@end
