//
//  FDAds.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDAds.h"

@implementation FDAds

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"title"        : @"title",
             @"content"      : @"desc",
             @"imageURL"     : @"image_url",
             @"linkURL"      : @"link_url",
             };
}


@end
