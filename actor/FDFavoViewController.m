//
//  FDFavoViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/23.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDFavoViewController.h"
#import "FDDefaultInfoView.h"

@implementation FDFavoViewController {
    FDDefaultInfoView *_infoView;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"心愿单";
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self loadNotificationCallback];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView.mj_header beginRefreshing];
}

- (void)loadNotificationCallback {
    
    WeakSelf;
    
    self.upRefreshCallback = ^(NSArray *objects, FDBaseModel *model, NSError *error) {
        
        StrongSelf;
        
        if (!objects.count) {
            
            [s_self loadPlaceholdViewIfNeed];
            
        } else {
            
            [s_self->_infoView dismiss];
        }
        
    };
}

- (void)loadPlaceholdViewIfNeed {
    
    if (!_infoView) {
        
        _infoView = [[FDDefaultInfoView alloc] initWithHeadText:@"您还没有心愿单"
                                                        TipText:@"总有乐趣在身边"
                                                          Image:FDImageWithName(@"Normal_Placeholder")
                                                BackgounrdImage:FDImageWithName(@"Order_Trip_BG")];
    }
    
    [_infoView showWithView:self.navigationController.view Duration:0.0];
    [self.navigationController.view bringSubviewToFront:self.navigationController.navigationBar];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 300;
}

@end
