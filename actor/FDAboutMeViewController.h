//
//  FDAboutMeViewController.h
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseStaticCellController.h"
#import "FDUserAvatarCell.h"
#import "FDUser.h"

@interface FDAboutMeViewController : FDBaseStaticCellController

@property (nonatomic, strong) FDUser *user;

@end
