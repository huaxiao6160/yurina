//
//  FDTag.m
//  actor
//
//  Created by 王澍宇 on 16/3/22.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDTag.h"

@implementation FDTag

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name"      : @"name",
             @"imageURL"  : @"image_url",
             };
}

- (instancetype)init {
    if (self = [super init]) {
        self.name = @"";
    }
    return self;
}


@end
