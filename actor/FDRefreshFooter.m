//
//  FDRefreshFooter.m
//  actor
//
//  Created by 王澍宇 on 16/4/2.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDRefreshFooter.h"

@implementation FDRefreshFooter {
    UILabel *_textLabel;
    
    UIImageView *_icon;
}

- (void)prepare {
    [super prepare];
    
    self.mj_h = 50;
    
    _textLabel = [[UILabel alloc] init];
    _textLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
    _textLabel.textColor = ColorNormalNaviTitle;
    _textLabel.textAlignment = NSTextAlignmentCenter;
    
    _icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Normal_Footer"]];
    _icon.contentMode = UIViewContentModeScaleAspectFit;
    
    _icon.hidden = YES;
    
    [self addSubview:_icon];
    [self addSubview:_textLabel];
}

- (void)placeSubviews {
    [super placeSubviews];
    
    _textLabel.center = self.center;
    _textLabel.frame  = self.bounds;
    
    _icon.center  = CGPointMake(self.center.x, self.center.y + 20);
}

- (void)setState:(MJRefreshState)state {
    
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
            _textLabel.text = @"上拉加载更多";
            _textLabel.hidden = NO;
            _icon.hidden = YES;
            break;
        case MJRefreshStateRefreshing:
            _textLabel.text = @"正在加载更多";
            _textLabel.hidden = NO;
            _icon.hidden = YES;
            break;
        case MJRefreshStateNoMoreData:
            _icon.hidden = NO;
            _textLabel.hidden = YES;
            break;
        default:
            break;
    }
}

@end
