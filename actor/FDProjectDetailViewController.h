//
//  FDProjectDetailViewController.h
//  actor
//
//  Created by 王澍宇 on 16/4/6.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseViewController.h"
#import "FDProject.h"

@interface FDProjectDetailViewController : FDBaseViewController

@property (nonatomic, strong) FDProject *project;

@end
