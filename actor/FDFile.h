//
//  FDFile.h
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"

@interface FDFile : FDBaseModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *fileName;

@property (nonatomic, strong) NSData *data;

@property (nonatomic, strong) NSString *mimeType;

@end
