//
//  FDButton.m
//  maruko
//
//  Created by 王澍宇 on 16/2/23.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import "FDButton.h"

@implementation FDButton {
}

+ (instancetype)buttonWithType:(UIButtonType)buttonType ActionBlock:(FDButtonAction)actionBlock {
    FDButton *button = [FDButton buttonWithType:buttonType];
    
    [button setActionBlock:actionBlock];
    
    return button;
}

+ (instancetype)buttonWithType:(UIButtonType)buttonType Title:(NSString *)title FontSize:(CGFloat)fontSize ActionBlock:(FDButtonAction)actionBlock {
    FDButton *button = [FDButton buttonWithType:buttonType ActionBlock:actionBlock];
    
    [button setTitleColor:ColorNormalButtonTitle forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:fontSize]];
    [button setTitle:title forState:UIControlStateNormal];
    
    return button;
}

- (void)doAction {
    
    if (_actionBlock) {
        _actionBlock(self);
    }
}

- (void)setActionBlock:(FDButtonAction)actionBlock {
    _actionBlock = actionBlock;
    [self removeTarget:self action:@selector(doAction) forControlEvents:UIControlEventTouchUpInside];
    [self addTarget:self action:@selector(doAction) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setImageName:(NSString *)imageName AutoHighlight:(BOOL)autoHighlight AutoDisabled:(BOOL)autoDisabled AutoSelected:(BOOL)autoSelected {
    
    [self setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    if (autoHighlight) {
        [self setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_H", imageName]] forState:UIControlStateHighlighted];
    }
    
    if (autoDisabled) {
        [self setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_D", imageName]] forState:UIControlStateDisabled];
    }
    
    if (autoSelected) {
        [self setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_S", imageName]] forState:UIControlStateSelected];
    }
}


- (void)setBackgroundImageName:(NSString *)imageName AutoHighlight:(BOOL)autoHighlight AutoDisabled:(BOOL)autoDisabled AutoSelected:(BOOL)autoSelected {
    
    [self setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    if (autoHighlight) {
        [self setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_H", imageName]] forState:UIControlStateHighlighted];
    }
    
    if (autoDisabled) {
        [self setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_D", imageName]] forState:UIControlStateDisabled];
    }
    
    if (autoSelected) {
        [self setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_S", imageName]] forState:UIControlStateSelected];
    }
}

@end
