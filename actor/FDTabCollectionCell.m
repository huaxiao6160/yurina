//
//  FDTabCollectionCell.m
//  asuka
//
//  Created by 王澍宇 on 16/3/6.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import "FDTabCollectionCell.h"

@implementation FDTabCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _imageView = [UIImageView new];
        _textLabel = [UILabel labelWithText:@"" Color:ColorCellText FontSize:11 Alignment:NSTextAlignmentCenter Light:YES];
        
        [self.contentView addSubview:_imageView];
        [self.contentView addSubview:_textLabel];
        
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(@0);
            make.size.mas_equalTo(CGSizeMake(50, 50));
        }];
        
        [_textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_imageView.mas_bottom).offset(5);
            make.left.right.bottom.equalTo(@0);
        }];
        
    }
    return self;
}

@end
