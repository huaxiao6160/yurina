//
//  FDPackage.m
//  actor
//
//  Created by 王澍宇 on 16/3/22.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDPackage.h"

@implementation FDPackage

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"objectID" : @"_id",
             @"name"     : @"name",
             @"fee"      : @"fee",
             @"duration" : @"duration",
             };
}

+ (NSValueTransformer *)objectIDJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [value stringValue];
    }];
}

- (instancetype)init {
    if (self = [super init]) {
        self.name = @"";
    }
    return self;
}


@end
