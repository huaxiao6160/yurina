//
//  FDProjectCheckoutView.h
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDInfoView.h"

#import "FDOrderService.h"
#import "FDProjectService.h"

@interface FDCheckoutView : FDInfoView

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info Type:(FDOrderCheckoutType)type;

@end
