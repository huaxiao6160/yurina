//
//  FDBaseViewController.h
//  maruko
//
//  Created by 王澍宇 on 16/2/21.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry.h>

#import "FDNavigationController.h"
#import "FDBaseViewModel.h"

#import "ComponentSheet.h"
#import "CategorySheet.h"
#import "LayoutSheet.h"
#import "ColorSheet.h"
#import "Marcos.h"

typedef enum : NSUInteger {
    FDNaviBarTypeRed,
    FDNaviBarTypeWhite,
    FDNaviBarTypeClear
} FDNaviBarType;

typedef void(^FDControllerCallback)(id object);

@interface FDBaseViewController : UIViewController <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIPanGestureRecognizer *backGestureRecognizer;

@property (nonatomic, strong) FDBaseViewModel *viewModel;

@property (nonatomic, strong) FDControllerCallback callback;

@property (nonatomic, strong) UIWindow *window;

- (void)updateNaviBarType:(FDNaviBarType)type;

- (void)goBack;

- (void)dismiss;

@end
