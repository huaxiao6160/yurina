//
//  FDTabCollectionCell.h
//  asuka
//
//  Created by 王澍宇 on 16/3/6.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import "FDBaseCollectionCell.h"

@interface FDTabCollectionCell : FDBaseCollectionCell

@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) UILabel *textLabel;

@end
