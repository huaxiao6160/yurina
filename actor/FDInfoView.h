//
//  FDInfoView.h
//  actor
//
//  Created by 王澍宇 on 16/4/3.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Masonry.h>

#import "Marcos.h"
#import "ColorSheet.h"
#import "LayoutSheet.h"
#import "CategorySheet.h"
#import "ComponentSheet.h"

@interface FDInfoView : UIView

@property (nonatomic, strong) FDButton *cancelButton;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *firstLine;

@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, strong) UIImageView *shadowView;

@property (nonatomic, strong) id info;

- (instancetype)initWithShadowColor:(UIColor *)color Info:(id)info;

- (void)showWithView:(UIView *)view;

- (void)showWithView:(UIView *)view Duration:(NSTimeInterval)duration;

- (void)dismiss;

- (void)moveHeigher;

- (void)moveLower;

@end
