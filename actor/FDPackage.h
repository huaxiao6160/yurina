//
//  FDPackage.h
//  actor
//
//  Created by 王澍宇 on 16/3/22.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"

@interface FDPackage : FDBaseModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, assign) float fee;

@property (nonatomic, assign) NSInteger duration;

@end
