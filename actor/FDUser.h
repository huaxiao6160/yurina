//
//  FDUser.h
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"

@interface FDUser : FDBaseModel

@property (nonatomic, strong) NSString *username;

@property (nonatomic, strong) NSString *nickname;

@property (nonatomic, strong) NSString *truename;

@property (nonatomic, strong) NSString *avatarURL;

@property (nonatomic, strong) NSArray<NSString *> *hobbies;

@end
