//
//  FDPwdViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/18.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDPwdViewController.h"
#import "FDAccountService.h"

@interface FDPwdViewController ()

@end

@implementation FDPwdViewController {
    
    UIImageView *_backgroundView;
    
    UITextField *_oldPwdField;
    UITextField *_newOnceField;
    UITextField *_newTwiceField;
    
    FDButton *_confirmButton;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"修改密码";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : ColorNormalBGWhite,
                                                                      NSFontAttributeName : [UIFont systemFontOfSize:20 weight:UIFontWeightRegular]}];
    [self.navigationItem.leftBarButtonItem setTintColor:ColorWelcomeTextMain];
    
    WeakSelf;
    
    _backgroundView = [[UIImageView alloc] initWithImage:FDImageWithName(@"Account_Register_BG")];
    
    _oldPwdField = [UITextField new];
    _oldPwdField.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    _oldPwdField.textColor = ColorAccountTextMain;
    _oldPwdField.secureTextEntry = YES;
    _oldPwdField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"输入旧密码" Color:ColorAccountPlacehodler];
    _oldPwdField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _oldPwdField.leftViewMode = UITextFieldViewModeAlways;
    _oldPwdField.background = FDImageWithName(@"Account_Field_Border");
    
    _newOnceField = [UITextField new];
    _newOnceField.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    _newOnceField.textColor = ColorAccountTextMain;
    _newOnceField.secureTextEntry = YES;
    _newOnceField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"输入新密码" Color:ColorAccountPlacehodler];
    _newOnceField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _newOnceField.leftViewMode = UITextFieldViewModeAlways;
    _newOnceField.background = FDImageWithName(@"Account_Field_Border");
    
    _newTwiceField = [UITextField new];
    _newTwiceField.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    _newTwiceField.textColor = ColorAccountTextMain;
    _newTwiceField.secureTextEntry = YES;
    _newTwiceField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"再次输入新密码" Color:ColorAccountPlacehodler];
    _newTwiceField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _newTwiceField.leftViewMode = UITextFieldViewModeAlways;
    _newTwiceField.background = FDImageWithName(@"Account_Field_Border");

    _newTwiceField.editingAction = ^(NSString *text, UITextField *field) {
        StrongSelf;
        [s_self->_confirmButton setEnabled:text.length ? YES : NO];
    };
    
    _confirmButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"确定" FontSize:18 ActionBlock:^(FDButton *button) {
        StrongSelf;
        
        NSString *oldPwd = s_self->_oldPwdField.text;
        NSString *newPwd = s_self->_newTwiceField.text;
        
        if (![s_self->_newOnceField.text isEqualToString:s_self->_newTwiceField.text]) {
            return [FDAlert alertWithTitle:@"错误" Message:@"新密码两次输入不相同"];
        }
        
        if ([s_self checkIfPwdLegal:newPwd]) {
            [FDAccountService changePwdWithOldPwd:oldPwd NewPwd:newPwd Callback:^(BOOL success) {
                if (success) {
                    [s_self dismissViewControllerAnimated:YES completion:nil];
                }
            }];
        }
        
    }];
    
    [_confirmButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:YES AutoSelected:NO];
    [_confirmButton setTitleColor:ColorAccountTextMain forState:UIControlStateNormal];
    [_confirmButton setEnabled:NO];
    
    [self.view addSubview:_backgroundView];
    [self.view addSubview:_oldPwdField];
    [self.view addSubview:_newOnceField];
    [self.view addSubview:_newTwiceField];
    [self.view addSubview:_confirmButton];
    
    [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(@0);
    }];
    
    [_oldPwdField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(SCREEN_HEIGHT / 4));
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@40);
    }];
    
    [_newOnceField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_oldPwdField.mas_bottom).offset(10);
        make.left.equalTo(_oldPwdField);
        make.right.equalTo(_oldPwdField);
        make.height.equalTo(_oldPwdField);
    }];
    
    [_newTwiceField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_newOnceField.mas_bottom).offset(10);
        make.left.equalTo(_oldPwdField);
        make.right.equalTo(_oldPwdField);
        make.height.equalTo(_oldPwdField);
    }];
    
    [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_newTwiceField.mas_bottom).offset(30);
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@45);
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateNaviBarType:FDNaviBarTypeClear];
}

- (BOOL)checkIfPwdLegal:(NSString *)pwd {
    
    if (![FDValidater validatePassword:pwd]) {
        [FDAlert alertWithTitle:@"错误" Message:@"密码长度不合法"];
        return NO;
    }
    
    return YES;
}

@end
