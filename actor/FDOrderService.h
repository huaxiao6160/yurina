//
//  FDOrderService.h
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import <Pingpp.h>

#import "FDWebService.h"
#import "FDCheckout.h"

typedef enum : NSUInteger {
    FDOrderChannelTypeWechat,
    FDOrderChannelTypeAlipay,
} FDOrderChannelType;

typedef enum : NSUInteger {
    FDOrderCheckoutTypeApplyFee,
    FDOrderCheckoutTypeProjectFee,
} FDOrderCheckoutType;

@interface FDOrderService : FDWebService

+ (void)getCheckoutWithOrderID:(NSString *)orderID Callback:(void(^)(BOOL success, FDCheckout *checkout))callback;

+ (void)checkoutFee:(CGFloat)fee Type:(FDOrderCheckoutType)type WithCallback:(void(^)(BOOL success, NSString *orderID))callback;

+ (void)refundWithOrderID:(NSString *)orderID Callback:(FDWebServiceCallback)callback;

+ (NSMutableDictionary *)applyParms;

@end
