//
//  FDSearchViewController.h
//  actor
//
//  Created by 王澍宇 on 16/3/29.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseTableViewController.h"

@interface FDSearchViewController : FDBaseTableViewController

@property (nonatomic, strong) UIView *searchBar;

@property (nonatomic, strong) NSString *keyword;

@property (nonatomic, assign) CGFloat rowHeight;

@end
