//
//  CPPickerView.h
//  akagi
//
//  Created by 王澍宇 on 15/10/14.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FDButton;
@class FDBaseViewController;

typedef void(^FDPickerCallback)(NSArray *pickedObjects);

@interface FDPickerView : UIView

@property (nonatomic, strong) UIPickerView *pickerView;

@property (nonatomic, strong) FDButton *cancelButton;

@property (nonatomic, strong) FDButton *finishButton;

@property (nonatomic, strong) NSArray *pickerObjects;

@property (nonatomic, strong) FDPickerCallback callback;

- (instancetype)initWithObjects:(NSArray *)array containerController:(FDBaseViewController *)controller;

- (void)show;

- (void)hide;

@end
