//
//  FDBanner.h
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"

@interface FDBanner : FDBaseModel

@property (nonatomic, strong) NSString *imageURL;

@property (nonatomic, strong) NSString *linkURL;

@end
